# Mosaic common library

Mosaic common library contain reusable code for PHP projects.


## Components

- Symfony
- DDD


## Versioning

This library uses [semantic versioning](http://semver.org). 



#### Tagging

List all repository tags

`$ git tag`

Add new tag

```
$ git tag
$ git tag -a 1.0.1 -m "1.0.1"`
$ git push --tags
```

