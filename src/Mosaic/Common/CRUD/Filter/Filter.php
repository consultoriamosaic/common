<?php


namespace Mosaic\Common\CRUD\Filter;


abstract class Filter
{
    private $page;

    private $numItems;

    /**
     * Filter constructor.
     * @param int $page
     * @param int $numItems
     */
    public function __construct($page = 0, $numItems = 10)
    {
        $this->page = $page;
        $this->numItems = $numItems;
    }

    /**
     * @return mixed
     */
    public function page()
    {
        return $this->page;
    }

    /**
     * @return mixed
     */
    public function numItems()
    {
        return $this->numItems;
    }


}