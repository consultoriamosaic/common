<?php

namespace Mosaic\Common\CRUD\Filter;

use Doctrine\Common\Collections\ArrayCollection;

class FilterResult
{
    /**
     * @var integer
     */
    public $total;

    public $start;

    public $end;

    public $pageSize;

    public $page;

    /**
     * @var ArrayCollection
     */
    public $elements;

    /**
     * FilterResult constructor.
     * @param $total
     * @param ArrayCollection $elements
     * @param $page
     * @param $pageSize
     * @param $start
     * @internal param int $totalElements
     */
    public function __construct(
        $total,
        ArrayCollection $elements,
        $page,
        $pageSize,
        $start
    ) {
        $this->total = $total;
        $this->elements = $elements;
        $this->page = $page;
        $this->start = empty($start) ? 1 : $start + 1;
        $this->end = $this->start + (count($elements) - 1);
        $this->pageSize = $pageSize;
        $this->numPages = 1;

        if ($this->total > 0) {
            $this->numPages = ceil($this->total / ($this->pageSize > 0 ? $this->pageSize : $this->total));
        }
    }

    /**
     * @return bool
     */
    public function isFirstPage()
    {
        return $this->page === 1;
    }

    /**
     * @return bool
     */
    public function isLastPage()
    {
        return $this->page === floor($this->total / $this->pageSize);
    }

    /**
     * @return int
     */
    public function firstPage()
    {
        return 1;
    }

    /**
     * @return float
     */
    public function lastPage()
    {
        return $this->numPages;
    }

    /**
     * @return mixed
     */
    public function prevPage()
    {
        return max(1, $this->page - 1);
    }

    /**
     * @return mixed
     */
    public function nextPage()
    {
        return min($this->page + 1, $this->numPages);
    }

    /**
     * @param $page
     * @return bool
     */
    public function currentPage($page)
    {
        return $this->page == $page;
    }

    /**
     * @return array
     */
    public function pages()
    {
        return $this->numPages > 1 ? range(max(1, $this->page - 5),min($this->page + 5, $this->numPages)): [1];
    }


}
