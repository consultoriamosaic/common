<?php


namespace Mosaic\Common\CRUD\Filter;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class FiltrableEntityRepository extends EntityRepository
{
    /**
     * @param Filter $filter
     * @return FilterResult
     */
    public function byFilter(Filter $filter)
    {

        $start = 0;
        $query = $this->byFilterQuery($filter);

        if ($filter->page() > 0) {
            $start = ($filter->numItems() * ($filter->page() - 1));
            $query->setFirstResult($start);
            $query->setMaxResults($filter->numItems());
        }

        $paginator = new Paginator($query);
        $entities = new ArrayCollection();
        count($paginator);

        foreach ($paginator as $entity) {
            $entities->add($entity);
        }

        return new FilterResult(count($paginator), $entities, $filter->page(), $filter->numItems(), $start);
    }

    /**
     * @param Filter $filter
     * @return Query
     */
    abstract protected function byFilterQuery(Filter $filter);
}