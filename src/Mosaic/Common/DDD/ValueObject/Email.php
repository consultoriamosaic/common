<?php


namespace Mosaic\Common\DDD\ValueObject;


class Email
{
    private $value;

    /**
     * Returns an Email object
     *
     * @param string $value
     */
    public function __construct($value)
    {
        $filteredValue = filter_var($value, FILTER_VALIDATE_EMAIL);
        if ($filteredValue === false) {
            throw new \InvalidArgumentException(sprintf("Invalid e-mail address: %s",$value));
        }
        $this->value = $filteredValue;
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}