<?php

namespace Mosaic\Common\DDD\Event;

interface DomainEventSubscriberInterface
{
    /**
     * @param DomainEventInterface $event
     * @return bool
     */
    public function isSubscribedTo(DomainEventInterface $event);

    /**
     * @param DomainEventInterface $event
     */
    public function handle($event);
}
