<?php

namespace Mosaic\Common\DDD\Event;

interface DomainEventInterface
{
    /**
     * @return \DateTimeImmutable
     */
    public function occurredOn();
}
