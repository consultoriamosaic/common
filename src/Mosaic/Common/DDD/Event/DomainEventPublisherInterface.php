<?php

namespace Mosaic\Common\DDD\Event;

interface DomainEventPublisherInterface
{
    /**
     * @param DomainEventInterface $event
     */
    public function publish(DomainEventInterface $event);

    /**
     * @param DomainEventSubscriberInterface $domainEventSubscriber
     */
    public function addSubscriber(DomainEventSubscriberInterface $domainEventSubscriber);
}
