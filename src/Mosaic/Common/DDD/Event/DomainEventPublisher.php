<?php

namespace Mosaic\Common\DDD\Event;

class DomainEventPublisher implements DomainEventPublisherInterface
{
    /**
     * @var DomainEventPublisher
     */
    private static $instance;

    /**
     * @var DomainEventSubscriberInterface[]
     */
    private $subscribers;

    /**
     * @return DomainEventPublisher
     */
    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new DomainEventPublisher();
        }
        return self::$instance;
    }

    /**
     * @param DomainEventInterface $event
     */
    public function publish(DomainEventInterface $event)
    {
        if (count($this->subscribers) > 0) {
            foreach ($this->subscribers as $subscriber) {
                if ($subscriber->isSubscribedTo($event)) {
                    $subscriber->handle($event);
                }
            }
        }
    }

    /**
     * @param DomainEventSubscriberInterface $domainEventSubscriber
     */
    public function addSubscriber(
      DomainEventSubscriberInterface $domainEventSubscriber
    ) {
        $this->subscribers[] = $domainEventSubscriber;
    }

    /**
     * @param DomainEventSubscriberInterface[] $subscribers
     */
    public function addSubscribers(array $subscribers)
    {
        foreach ($subscribers as $subscriber) {
            $this->addSubscriber($subscriber);
        }
    }
}