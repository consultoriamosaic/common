<?php

namespace Mosaic\Common\Symfony\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsNifCifNieValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (empty($value)) {
            return;
        }

        $nif = strtoupper($value);

        $nifRegEx = '/^[0-9]{8}[A-Z]$/i';
        $nieRegEx = '/^[XYZ][0-9]{7}[A-Z]$/i';
        $cifRegEx1 = '/^[ABEH][0-9]{8}$/i';
        $cifRegEx2 = '/^[KPQS][0-9]{7}[A-J]$/i';
        $cifRegEx3 = '/^[CDFGJLMNRUVW][0-9]{7}[0-9A-J]$/i';

        $letras = "TRWAGMYFPDXBNJZSQVHLCKE";

        if (preg_match($nifRegEx, $nif)) {
            if ($letras[(substr($nif, 0, 8) % 23)] != $nif[8]) {
                $this->context->buildViolation("validation.invalid_nif_letter")
                    ->setInvalidValue($value)
                    ->addViolation();
            }
        } else {
            if (preg_match($nieRegEx, $nif)) {
                if ($nif[0] == "X") {
                    $nif[0] = "0";
                } else {
                    if ($nif[0] == "Y") {
                        $nif[0] = "1";
                    } else {
                        if ($nif[0] == "Z") {
                            $nif[0] = "2";
                        }
                    }
                }

                if ($letras[(substr($nif, 0, 8) % 23)] != $nif[8]) {
                    $this->context->buildViolation("validation.invalid_nie_letter")
                        ->setInvalidValue($value)
                        ->addViolation();
                }
            } else {
                if (preg_match($cifRegEx1, $nif) || preg_match($cifRegEx2,
                        $nif) || preg_match($cifRegEx3, $nif)
                ) {
                    $control = $nif[strlen($nif) - 1];
                    $suma_A = 0;
                    $suma_B = 0;

                    for ($i = 1; $i < 8; $i++) {
                        if ($i % 2 == 0) {
                            $suma_A += (int)$nif[$i];
                        } else {
                            $t = (int)$nif[$i] * 2;
                            $p = 0;

                            for ($j = 0; $j < strlen($t); $j++) {
                                $p += substr($t, $j, 1);
                            }
                            $suma_B += $p;
                        }
                    }

                    $suma_C = (int)($suma_A + $suma_B) . "";
                    $suma_D = (10 - (int)$suma_C[strlen($suma_C) - 1]) % 10;

                    $letras = "JABCDEFGHI";

                    if ($control >= "0" && $control <= "9") {
                        if ($control != $suma_D) {
                            $this->context->buildViolation("validation.invalid_cif_letter")
                                ->setInvalidValue($value)
                                ->addViolation();
                        }
                    } else {
                        if (strtoupper($control) != $letras[$suma_D]) {
                            $this->context->buildViolation("validation.invalid_cif_letter")
                                ->setInvalidValue($value)
                                ->addViolation();
                        }
                    }
                } else {
                    $this->context->buildViolation("validation.invalid_identity_number_format")
                        ->setInvalidValue($value)
                        ->addViolation();
                }
            }
        }
    }
}
