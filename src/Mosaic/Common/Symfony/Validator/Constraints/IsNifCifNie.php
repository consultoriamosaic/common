<?php

namespace Mosaic\Common\Symfony\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class IsNifCifNie extends Constraint
{
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}