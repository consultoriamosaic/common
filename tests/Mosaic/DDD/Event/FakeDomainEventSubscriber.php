<?php

namespace Tests\Mosaic\DDD\Event;

use Mosaic\Common\DDD\Event\DomainEventInterface;
use Mosaic\Common\DDD\Event\DomainEventSubscriberInterface;

class FakeDomainEventSubscriber implements DomainEventSubscriberInterface
{
    /**
     * @var DomainEventInterface[]
     */
    private $handledEvents;

    /**
     * @param DomainEventInterface $event
     * @return bool
     */
    public function isSubscribedTo(DomainEventInterface $event)
    {
        return get_class($event) === FakeDomainEvent::class;
    }

    /**
     * @param DomainEventInterface $event
     */
    public function handle($event)
    {
        $this->handledEvents[] = $event;
    }

    /**
     * @return DomainEventInterface[]
     */
    public function handledEvents()
    {
        return $this->handledEvents;
    }
}
