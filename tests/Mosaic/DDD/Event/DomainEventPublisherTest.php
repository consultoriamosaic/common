<?php

namespace Tests\Mosaic\DDD\Event;

use Mosaic\Common\DDD\Event\DomainEventPublisher;

class DomainEventPublisherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function publishAnEvent()
    {
        $subscriber = new FakeDomainEventSubscriber();
        DomainEventPublisher::instance()->addSubscriber($subscriber);

        DomainEventPublisher::instance()->publish(
            new FakeDomainEvent(new \DateTimeImmutable())
        );

        $this->assertCount(1, $subscriber->handledEvents());
        $this->assertInstanceOf(FakeDomainEvent::class, $subscriber->handledEvents()[0]);
    }
}
