<?php

namespace Tests\Mosaic\DDD\Event;

use Mosaic\Common\DDD\Event\DomainEventInterface;

class FakeDomainEvent implements DomainEventInterface
{
    /**
     * @var \DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @param \DateTimeImmutable $occurredOn
     */
    public function __construct(\DateTimeImmutable $occurredOn)
    {
        $this->occurredOn = $occurredOn;
    }

    /**
     * @return string
     */
    public function name()
    {
        return 'FakeDomainEvent';
    }

    /**
     * @return \DateTimeImmutable
     */
    public function occurredOn()
    {
        return $this->occurredOn();
    }
}